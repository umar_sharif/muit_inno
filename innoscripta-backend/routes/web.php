<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Article\NewsApiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::middleware('auth')->group(function () {

    Route::get('get-news-api-articles',[NewsApiController::class,'getAticleFromNewsApi']);
});
Route::get('/test', function () {
   dd('test');
});

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
