<?php

namespace App\Console\Commands;

use App\Enums\ApiTypes;
use App\Jobs\InsertNewsJob;
use App\NewsApi\NewsApiFactory;
use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CollectNewsCommand extends Command
{
    protected $signature = 'news:collect';
    protected $description = 'Collects and saves news data';

    public function handle()
    {

        $apiTypes = ApiTypes::toArray();
        foreach ($apiTypes as $apiType) {
            $newsApi = NewsApiFactory::createNewsApi($apiType);
            //dd($newsApi);
            $newsData = $newsApi->fetchNews();
//            dd($newsData);

                $modifiedNewsData = [];
            foreach ($newsData as $newsItem) {
                $newsItem['created_at'] = Carbon::now();
                $newsItem['updated_at'] = Carbon::now();
                $modifiedNewsData[] = $newsItem;

            }

//            Article::insert($modifiedNewsData);


                dispatch(new InsertNewsJob($modifiedNewsData));

        }

        $this->info('News data collected ');
    }
}
