<?php

namespace App\NewsApi;

use App\Services\NyTimesService;

class NyTimesApi implements NewsApiInterface
{
    public function fetchNews(): array
    {
       $articles=NyTimesService::getArticles();
       //dd($articles);
       return NyTimesService::processArticles($articles);
    }
}
