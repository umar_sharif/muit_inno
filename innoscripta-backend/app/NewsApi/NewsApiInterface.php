<?php

namespace App\NewsApi;

interface NewsApiInterface
{
    public function fetchNews(): array;
}
