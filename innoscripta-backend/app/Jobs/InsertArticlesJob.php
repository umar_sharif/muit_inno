<?php

namespace App\Jobs;

use App\Models\Article;
use App\Models\Author;
use App\Models\Category;
use App\Models\Source;
use App\Services\GrudiansService;
use App\Services\NewsAPIService;
use App\Services\NyTimesService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InsertArticlesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    private $newsApi, $nyTimes,$gurdians;

    public function __construct($newsApi, $nyTimes,$gurdians)
    {
        $this->newsApi = $newsApi;
        $this->nyTimes = $nyTimes;
        $this->gurdians=$gurdians;

    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        NewsAPIService::processArticles($this->newsApi);
        NyTimesService::processArticles($this->nyTimes);
        GrudiansService::processArticles($this->gurdians);
    }
}
