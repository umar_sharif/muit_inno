<?php

namespace App\Jobs;

use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InsertNewsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $newsItems;

    public function __construct($newsItem)
    {
        $this->newsItems = $newsItem;
//        dd($this->newsItems);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

       Article::insert($this->newsItems);

    }
}
