<?php

namespace App\Http\Controllers\Preference;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemCollection;
use App\Models\Author;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    use ApiResponse;
    public function index()
    {
        return $this->success(new ItemCollection(
            Author::all()
        ), 'Source Lists', 200);
    }
}
