<?php

namespace App\Http\Controllers\Preference;

use App\Http\Controllers\Controller;
use App\Http\Resources\PreferenceCollection;
use App\Models\Author;
use App\Models\Category;
use App\Models\Source;
use App\Models\User;
use App\Models\UserPreference;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserPreferenceController extends Controller
{
    use ApiResponse;

    public function index()
    {
        $user = Auth::user();
//dd(Source::with('preferences')->get());
        return $this->success(new PreferenceCollection($user->preferences()->with('preferenceable')->get()),
            'User Preferences List ', 200);

    }

    public function store(Request $request)
    {

        $user = auth()->user();

        $sources = $request->sources;
        $authors = $request->authors;
        $categories = $request->categories;

        if (!empty($sources)) {
            $this->saveUserPreferences($user, $sources, Source::class);

        }
        if (!empty($authors)) {
            $this->saveUserPreferences($user, $authors, Author::class);

        }
        if (!empty($categories)) {
            $this->saveUserPreferences($user, $categories, Category::class);

        }

        return $this->success(null, 'User Preferenced saved ', 201);
    }


//    private function saveUserPreferences(User $user, array $preferenceIds, string $preferenceType)
//    {
//        $preferences = $preferenceType::whereIn('id', $preferenceIds)->get();
//
//        foreach ($preferences as $preference) {
//            $userPreference = new UserPreference();
//            $userPreference->preferenceable()->associate($preference);
//            $user->userPreferences()->save($userPreference);
//        }
//    }

    private function saveUserPreferences(User $user, $preferenceIds, $preferenceType)
    {
        $existingPreferenceIds = $user->preferences()
            ->where('preferenceable_type', $preferenceType)
            ->pluck('preferenceable_id')
            ->toArray();

        //dd($existingPreferenceIds,$preferenceType);
        $newPreferenceIds = array_diff($preferenceIds, $existingPreferenceIds);

        $preferences = $preferenceType::whereIn('id', $newPreferenceIds)->get();

        $user->preferences()->createMany($preferences->map(function ($preference) {
            return [
                'preferenceable_id' => $preference->id,
                'preferenceable_type' => get_class($preference)
            ];
        }));
    }
}
