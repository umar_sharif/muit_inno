<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PreferenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {

        switch ($this->preferenceable_type){
            case 'App\Models\Source':
                $type='Sources';
                break;
            case 'App\Models\Author':
                $type='Author';
                break;
            case 'App\Models\Category':
                $type='Category';
                break;
            default:
                $type='';
                break;

        }

        return [
            'type'=>$type,
            'name'=>$this->preferenceable->name
        ];
    }
}
