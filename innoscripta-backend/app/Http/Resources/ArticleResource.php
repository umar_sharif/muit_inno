<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'source' => $this->source->name,
            'category' => $this->source->name,
            'author' => ($this->author) ? $this->author->name : 'Anonymous',
            'title' => $this->title,
            'description' => $this->description,
            'content' => $this->content,
            'date' =>$this->created_at->format('D-M-Y')

        ];
    }
}
