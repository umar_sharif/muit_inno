<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSavedArticle extends Model
{
    use HasFactory;
    protected $guarded=[];
}
