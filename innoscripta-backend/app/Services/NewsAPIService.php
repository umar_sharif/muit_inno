<?php

namespace App\Services;

use App\Models\Article;
use App\Models\Author;
use App\Models\Category;
use App\Models\Source;
use App\Traits\ApiResponse;
use GuzzleHttp\Client;
use function GuzzleHttp\json_decode;

class NewsAPIService
{

    use ApiResponse;

    //  implementation
//    private $httpClient;

//    public function __construct(Client $httpClient)
//    {
//        $this->httpClient = $httpClient;
//    }

    public static function getArticles()
    {
        $category = 'news';

        $httpClient = new \GuzzleHttp\Client();
        $newsData = [];

        $pages = range(1, 2);
        foreach ($pages as $page) {
            $response = $httpClient->request('GET', env('NEWSAPI_URL'), [
                'query' => [
                    'apiKey' => env('NEWSAPI_KEY'),
                    'q' => $category,
                    'page' => $page,
                ],
            ]);

            $responseArray = json_decode($response->getBody(), true)['articles'];
            $newsData = array_merge($newsData, $responseArray);
        }

        return ([
            'type' => 'News Api ',
            'data' => $newsData,
            'category' => $category
        ]);
    }


    //insert into db from news api
    public static function processArticles($articles)
    {
        $author = '';
        $newsData = [];

        foreach ($articles['data'] as $articleData) {
            // Save each article here

            // Example:
            // Retrieve or create the Source record
            $source = Source::firstOrCreate(['name' => $articles['type']]);

            // Retrieve or create the Category record
            $category = Category::firstOrCreate(['name' => $articles['category']]);

            (!isset($articleData['author'])) ? $author = null : $author = Author::firstOrCreate(['name' => $articleData['author']]);


            // Create the Article record
            $newsData[] = [
                'title' => $articleData['title'],
                'description' => $articleData['description'],
                'content' => $articleData['content'],
                'source_id' => $source->id,
                'category_id' => $category->id,
                'author_id' => ($author) ? $author->id : null,
                'published_at' => $articleData['publishedAt']
            ];


        }
        return $newsData;


    }


}
