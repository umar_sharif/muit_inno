<?php

namespace App\Services;

use App\Models\Article;
use App\Models\Author;
use App\Models\Category;
use App\Models\Source;
use App\Traits\ApiResponse;
use GuzzleHttp\Client;
use function GuzzleHttp\json_decode;

class GrudiansService
{

    use ApiResponse;
    //  implementation
//    private $httpClient;

//    public function __construct(Client $httpClient)
//    {
//        $this->httpClient = $httpClient;
//    }

    public static function getArticles()
    {

        $httpClient = new \GuzzleHttp\Client();
        $newsData = [];

        $pages = range(1, 3);
        foreach ($pages as $page) {
        $response = $httpClient->request('GET', env('GURDIANS_URL'), [
            'query' => [
                'api-key' => env('GURDIANS_KEY'),
                'page'=>1

            ],
        ]);

        $responseArray = json_decode($response->getBody(), true)['response']['results'];
        $newsData = array_merge($newsData, $responseArray);
    }

//        dd($responseArray);
        //return $responseArray;

        return ([
            //no explecit source or author name is given, so i used type for source and author

            'type' => 'The Guardians News ',
            'data' => $newsData,
            ]);
    }




  //insert into db from news api
    public static function processArticles($articles)
    {
//        dd($articles);

        $author = '';
        $newsData=[];

        foreach ($articles['data'] as $articleData) {
            // Save each article here


            // Example:
            // Retrieve or create the Source record
            $source = Source::firstOrCreate(['name' => $articles['type']]);

            // Retrieve or create the Category record
            $category = Category::firstOrCreate(['name' => $articleData['sectionName']]);

            $author = Author::firstOrCreate(['name' => $articles['type']]);


            $newsData[]=[
                'title' => $articleData['webTitle'],
                'description' => $articleData['webUrl'],
                'content' => $articleData['webUrl'],
                'source_id' => $source->id,
                'category_id' => $category->id,
                'author_id' => ($author) ? $author->id : null,
                'published_at' => $articleData['webPublicationDate'],
            ];

            // Create the Article record

            /*$article = Article::create([

            ]);*/
        }
        return $newsData;



    }


}
