<?php

namespace App\Services;

use App\Models\Article;
use App\Models\Author;
use App\Models\Category;
use App\Models\Source;
use GuzzleHttp\Client;
use function GuzzleHttp\json_decode;

class NyTimesService
{

//fetch article from nytimes api
    public static function getArticles()
    {
        $search = 'news';
        $httpClient = new \GuzzleHttp\Client();

        $newsData = [];

        $pages = range(1, 3);
        foreach ($pages as $page) {
            $response = $httpClient->request('GET', env('NYTIMES_URL'), [
                'query' => [
                    'api-key' => env('NYTIMES_KEY'),
                    'q' => 'news' or 'war' or 'sports',
                    'page' => $page,
                ],
            ]);

            $responseArray = json_decode($response->getBody(), true)['response']['docs'];

            $newsData = array_merge($newsData, $responseArray);
        }

        return [
            'type' => 'Ny times',
            'data' => $newsData,
            'category' => $search
        ];
    }

    public static function processArticles($articles)
    {
//        dd($articles);

        $author = '';

        foreach ($articles['data'] as $articleData) {


            // Example:
            // Retrieve or create the Source record
//            dd($articleData['source']);

            //source get from api

            $source = Source::firstOrCreate(['name' => $articleData['source']]);

            // Retrieve or create the Category record
            $category = Category::firstOrCreate(['name' => $articleData['section_name']]);

            //as no author name is given so i used source name same as author name
            $author = Author::firstOrCreate(['name' => $articleData['source']]);


            // Create the Article record

            $newsData[]=[
                'title' => $articleData['headline']['main'],
                'description' => $articleData['snippet'],
                'content' => $articleData['lead_paragraph'],
                'source_id' => $source->id,
                'category_id' => $category->id,
                'author_id' => ($author) ? $author->id : null,
                'published_at' => now(),
            ];

        }
        return $newsData;

    }


}
