import { applyMiddleware, combineReducers, createStore } from "redux";

import thunk from "redux-thunk";
import authReducer from "../services/reducer/authReducer";
import preferencesReducer from "../services/reducer/preferenceReducer";
import articleReducer from "../services/reducer/articleReducer";
import sourcesReducer from "../services/reducer/sourcesReducer";
import authorsReducer from "../services/reducer/authorsReducer";
import categoriesReducer from "../services/reducer/categoriesReducer";

const mainReducer = combineReducers({

    authReducer,
    preferencesReducer,
    sourcesReducer,
    authorsReducer,
    categoriesReducer,
    articleReducer,
});

const store = createStore(mainReducer, applyMiddleware(thunk));

export default store;
