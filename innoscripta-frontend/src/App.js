import { useSelector } from "react-redux";
import LoginForm from "./components/Forms/LoginForm";
import RegistrationForm from "./components/Forms/RegistrationForm";
import Header from "./components/Layout/Header";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import UserPreferenceContainer from "./components/Preference/UserPreferenceContainer";
import NewsFeed from "./components/NewsFeed/NewsFeed";



function App() {

  const {
    authReducer: { error, isAuthenticated },
  } = useSelector((state) => state);
 
 
 
 
  return (
    <div className="bg-blue-100">
      <Router>
        <Header />

        <Routes>
          <Route path="/login" element={<LoginForm  />} />
          <Route path="/register" element={<RegistrationForm />} />
          <Route path="/preference" element={<UserPreferenceContainer />} />
          <Route  path="/" element={<NewsFeed />} />

          <Route
            path="*"
            element={() => {
              <h2>Not Found</h2>;
            }}
          />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
