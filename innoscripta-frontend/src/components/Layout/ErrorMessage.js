import React from 'react'

const ErrorMessage = ({message}) => {
  return (
    <div className="bg-red-500 text-white font-bold rounded-md py-2 px-4">
      {message}
    </div>
  );
}

export default ErrorMessage
