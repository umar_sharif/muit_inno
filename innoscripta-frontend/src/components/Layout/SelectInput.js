import React from 'react'

const SelectInput = ({options}) => {
  return (
    <div className="w-1/2">
      <Select
        options={categories}
        getOptionLabel={(category) => category.name}
        getOptionValue={(category) => category.id}
        isMulti
        placeholder="Select category"
        className="w-full"
        onChange={handleCategoryChange}
        value={selectedCategories}
      />
    </div>
  );
}

export default SelectInput
