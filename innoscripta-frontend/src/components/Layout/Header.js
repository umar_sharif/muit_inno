import React, { useEffect, useState } from "react";
import CustomButton from "./CustomButton";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { logoutAsyncAction } from "../../services/actions/authActions";



const Header = () => {
 
  // const { authReducer: { isAuthenticated } } = useSelector((state) => state);
  const token= localStorage.getItem('token');

  const dispatch = useDispatch();

  const handleLogout = () => {
   
    console.log('logout');
   
    dispatch(logoutAsyncAction())
  
  };
  useEffect(() => {
    console.log("token", token);
  });

  return (
    <header className="bg-gray-200 py-4">
      <nav className="container mx-auto">
        <ul className="flex justify-between items-center">
          <li>
            <Link to="/" className="text-gray-800">
              NewsFeed
            </Link>
          </li>
          <li>
           {token && <Link to="/preference" className="text-gray-800">
              Set Preference
            </Link>}
          </li>
          <li>
            {token ? (
              <div className="relative inline-block">
                <CustomButton
                  onClick={handleLogout}
                  bgColor="bg-red-500"
                  hoverColor="hover:bg-red-700"
                  text="Log out"
                />
              </div>
            ) : (
              <div>
                <Link to="/login">
                  <CustomButton
                    bgColor="bg-blue-500"
                    hoverColor="hover:bg-blue-700"
                    text="Sign In "
                  />
                </Link>
                <Link to="/register">
                  <CustomButton
                    bgColor="bg-green-500"
                    hoverColor="hover:bg-green-700"
                    text="Sign Up"
                  />
                </Link>
              </div>
            )}
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
