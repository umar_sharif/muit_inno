import React from "react";

const SearchInput = ({ value, onChange }) => {
  return (
    <div className="my-4">
      <input
        type="text"
        value={value}
        onChange={onChange}
        placeholder="Search"
        className="w-full p-2 border border-gray-300 rounded-md"
      />
    </div>
  );
};

export default SearchInput;
