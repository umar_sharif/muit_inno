import React, { useEffect } from "react";

const PreferenceList = ({ preferences }) => {

  return (
    <div>
      <h2 className="text-xl font-bold mb-4 text-center">Your Preferences</h2>
      <div className="flex flex-wrap">
        {preferences.map((preference, index) => (
          <div key={index} className="w-full sm:w-1/2 lg:w-1/3 p-2">
            <div className="bg-white rounded-md shadow-md p-4 mb-4">
              <h3 className="text-lg font-bold mb-2">{preference.type}</h3>
              <p className="text-gray-600">{preference.name}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default PreferenceList;
