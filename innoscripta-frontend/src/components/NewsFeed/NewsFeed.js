import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchAuthors,
  fetchCategories,
  fetchPreferences,
  fetchSources,
} from "../../services/actions/preferenceAction";
import fetchArticles from "../../services/actions/articleActions";
import Filters from "./Filters";
import Article from "./Article";
import PreferenceList from "../Preference/PreferenceList";
import PaginationComponent from "../Layout/PaginationComponent";

const NewsFeed = () => {
  const dispatch = useDispatch();
  const [searchTerm, setSearchTerm] = useState("");
  const [selectedSourcesIds, setSelectedSourcesIds] = useState([]);
  const [selectedCategoriesIds, setSelectedCategoriesIds] = useState([]);
  const [selectedDate, setSelectedDate] = useState(null);
 
 
  const {
    articleReducer: { articles, loading, meta, links },
    preferencesReducer: { preferences },
    sourcesReducer: { sources },
    authorsReducer: { authors },
    authReducer: { isAuthenticated },
    categoriesReducer: { categories },
  } = useSelector((state) => state);

  useEffect(() => {
    dispatch(fetchSources());
    dispatch(fetchCategories());
    dispatch(fetchArticles());
    dispatch(fetchPreferences());
  }, []);
  const handleFilterSubmit = () => {
    dispatch(
      fetchArticles(
        searchTerm,
        selectedSourcesIds,
        selectedCategoriesIds,
        selectedDate
      )
    );
  };
  const handlePageChange = (page) => {
    dispatch(fetchArticles(searchTerm, selectedSourcesIds, selectedCategoriesIds, selectedDate, page));
  };
  
  return (
    <div className="container mx-auto p-4">
      {isAuthenticated && <PreferenceList preferences={preferences} />}

      <Filters
        sources={sources}
        categories={categories}
        searchTerm={searchTerm}
        setSearchTerm={setSearchTerm}
        setSelectedCategoriesIds={setSelectedCategoriesIds}
        setSelectedSourcesIds={setSelectedSourcesIds}
        selectedDate={selectedDate}
        setSelectedDate={setSelectedDate}
        handleSubmit={handleFilterSubmit}
      />

      <div className="my-4">
        {articles.map((article, index) => {
          return (
            <Article
              key={index}
              title={article.title}
              description={article.description}
              content={article.content}
              date={article.date}
              source={article.source}
              category={article.category}
              author={article.author}
            />
          );
        })}
        <PaginationComponent meta={meta} onPageChange={handlePageChange} />
      </div>
    </div>
  );
};

export default NewsFeed;
