import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import {
  
  errorLoginAction,
  errorRegAction, loginAction,
} from "../../services/actions/authActions.js";
import { BASE_URL } from "../../services/constants/constants";
import CustomButton from "../Layout/CustomButton";
import ErrorMessage from "../Layout/ErrorMessage.js";
import { Link, useNavigate } from "react-router-dom";

const LoginForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const {
    authReducer: { errorLogin },
  } = useSelector((state) => state);

  const handleLogin = async () => {
    try {
      const response = await axios.post(`${BASE_URL}api/login`, {
        email,
        password,
      });

      console.log("test", response.data.data.token);

      dispatch(loginAction(response.data.data.token));
      
      //redirection 

      navigate('/');


    } catch (error) {
      console.log("Error:", error.response.data.message);

      dispatch(errorLoginAction(error.response.data.message));
    }

    setEmail("");
    setPassword("");
  };

  return (
    <div className="flex justify-center items-center h-screen bg-gray-100">
      <div className="max-w-md p-6 bg-white rounded-lg shadow-md">
        <h2 className="text-2xl font-bold mb-4">Login</h2>

        {errorLogin && <ErrorMessage message={errorLogin} />}

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="email"
          >
            Email
          </label>
          <input
            className="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="email"
            type="email"
            placeholder="Enter your email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="mb-6">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="password"
          >
            Password
          </label>
          <input
            className="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="password"
            type="password"
            placeholder="Enter your password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="flex justify-center ">
          <CustomButton
            bgColor="bg-blue-500"
            hoverColor="hover:bg-blue-900"
            text="Sign In"
            onClick={handleLogin}
          />
          <Link to="/register">
            <CustomButton
              bgColor="bg-green-500"
              hoverColor="hover:bg-green-700"
              text="Sign Up"
            />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default LoginForm;
