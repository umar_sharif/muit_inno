import {
  FETCH_AUTHORS_SUCCESS,
  FETCH_AUTHORS_FAILURE,
} from "../constants/constants";

const initialState = {
  authors: [],
  error: null,
};

const authorsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_AUTHORS_SUCCESS:
      return {
        ...state,
        authors: action.authors,
        error: null,
      };
    case FETCH_AUTHORS_FAILURE:
      return {
        ...state,
        authors: [],
        error: action.error,
      };
    default:
      return state;
  }
};

export default authorsReducer;
