
import {
  FETCH_ARTICLES_REQUEST,
  FETCH_ARTICLES_SUCCESS,
  FETCH_ARTICLES_FAILURE,
} from "../constants/constants";


const initialState = {
  articles: [],
  loading: false,
  meta: {},
  links:{},
  error: null
};

// Reducer
const articleReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ARTICLES_REQUEST:
      return {
        ...state,
        loading: true,
        error: null
      };
    case FETCH_ARTICLES_SUCCESS:
      console.log('FETCH_ARTICLES_SUCCess',action.payload);
      return {
        ...state,
        loading: false,
        articles: action.payload.data.article,
        meta: action.payload.meta,
        links: action.payload.links,
      };
    case FETCH_ARTICLES_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    default:
      return state;
  }
};

export default articleReducer;