import {
  FETCH_PREFERENCES,
  FETCH_PREFERENCES_SUCCESS,
  FETCH_PREFERENCES_FAILURE,
} from "../constants/constants";

const initialState = {
  preferences: [],
  loading: false,
  error: null,
};

const preferenceReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PREFERENCES:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_PREFERENCES_SUCCESS:
      return {
        ...state,
        preferences: action.payload,
        loading: false,
        error: null,
      };
    case FETCH_PREFERENCES_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default preferenceReducer;
