import React from "react";
import {
  LOGIN_FAILURE,
  REGISTER_FAILURE,
  REGISTER_SUCCESS,
  USER_LOGIN,
  USER_LOGOUT,
  USER_REGISTERATION,
} from "../constants/constants";

const initialState = {
  token: null,
  isAuthenticated: false,
  loading: false,
  errorReg: null,
  errorLogin: null,
};
const authReducer = (state = initialState, action) => {


  switch (action.type) {
    case USER_LOGIN:


      localStorage.setItem("token", action.payload);

      return {
        ...state,
        token: action.payload,
        isAuthenticated: true,
      };

    case LOGIN_FAILURE:
      return {
        ...state,
        loading: false,
        errorLogin: action.payload,
        isAuthenticated: false,
      };

    case USER_LOGOUT:

      localStorage.removeItem("token");


      return {
        ...state,
        isAuthenticated: false,
        token: null,
      
      };

    case USER_REGISTERATION:
      console.log(action.type);

      console.log(`reg`, state);

      return {
        ...state,
        loading: true,
        errorReg: null,
      };

    case REGISTER_SUCCESS:
      localStorage.setItem("token", action.payload);

      return {
        ...state,
        token: action.payload,
        isAuthenticated: true,
        loading: false,
        errorReg: null,
      };

    case REGISTER_FAILURE:
      return {
        ...state,
        loading: false,
        errorReg: action.payload,
        isAuthenticated: false,
      };

    default:
      return state;
  }
};

export default authReducer;
