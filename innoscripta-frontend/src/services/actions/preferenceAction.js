import axios from "axios";

import {
  BASE_URL,
  FETCH_SOURCES_SUCCESS,
  FETCH_SOURCES_FAILURE,
  FETCH_AUTHORS_SUCCESS,
  FETCH_AUTHORS_FAILURE,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_CATEGORIES_FAILURE,
  FETCH_PREFERENCES,
  FETCH_PREFERENCES_FAILURE,
  FETCH_PREFERENCES_SUCCESS,
} from "../constants/constants";
import { apiCall } from "../../api";

export const fetchSources = () => {
  return async (dispatch) => {
    try {
      const response = await axios.get(`${BASE_URL}api/get-sources`);
      dispatch({ type: FETCH_SOURCES_SUCCESS, sources: response.data.data.item });
    } catch (error) {
      dispatch({ type: FETCH_SOURCES_FAILURE, error: error.message });
    }
  };
};

export const fetchAuthors = () => {
  return async (dispatch) => {
    try {
      const response = await axios.get(`${BASE_URL}api/get-authors`);
      dispatch({ type: FETCH_AUTHORS_SUCCESS, authors: response.data.data.item });
    } catch (error) {
      dispatch({ type: FETCH_AUTHORS_FAILURE, error: error.message });
    }
  };
};

export const fetchCategories = () => {
  return async (dispatch) => {
    try {
      const response = await axios.get(`${BASE_URL}api/get-categories`);
      dispatch({ type: FETCH_CATEGORIES_SUCCESS, categories: response.data.data.item });
    } catch (error) {
      dispatch({ type: FETCH_CATEGORIES_FAILURE, error: error.message });
    }
  };
};

export const fetchPreferences = () => {
  return async (dispatch) => {
    dispatch({ type: FETCH_PREFERENCES });

    try {
      const response = await apiCall('api/get-preferences','GET')
      dispatch({
        type: FETCH_PREFERENCES_SUCCESS,
        payload: response.data.item,
      });
    } catch (error) {
      dispatch({
        type: FETCH_PREFERENCES_FAILURE,
        payload: error.message,
      });
    }
  }
  };
