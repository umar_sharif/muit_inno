import {
  BASE_URL,
  LOGIN_FAILURE,
  REGISTER_FAILURE,
  REGISTER_SUCCESS,
  USER_LOGIN,
  USER_LOGOUT,
  USER_REGISTERATION,
} from "../constants/constants";
import axios from "axios";
import { apiCall } from "../../api";

export const loginAction = (token) => {
  return {
    type: USER_LOGIN,
    payload: token,
  };
};

export const logoutAction = () => {
  console.log("logout action");
  return {
    type: USER_LOGOUT,
  };
};

const registrationAction = () => {
  console.log("registration action");
  return {
    type: USER_REGISTERATION,
  };
};

const successRegAction = (data) => {
  return {
    type: REGISTER_SUCCESS,
    payload: data,
  };
};

export const errorRegAction = (error) => {
  return {
    type: REGISTER_FAILURE,
    payload: error,
  };
};
export const errorLoginAction = (error) => {
  return {
    type: LOGIN_FAILURE,
    payload: error,
  };
};

export const logoutAsyncAction = () => async (dispatch) => {
  try {
    dispatch(logoutAction());

    await apiCall("api/logout", "GET");
  } catch (error) {
    console.error("Error logging out :", error);
  }
};

const registerUser = (userData) => async (dispatch) => {
    
  dispatch(registrationAction());

  try {
  
    const response = await axios.post(`${BASE_URL}api/register`, userData);

    dispatch(successRegAction(response.data.data.token));
  
  } catch (error) {
   
    dispatch(errorRegAction(error.response.data.message));
  
  }
};

export default registerUser;
