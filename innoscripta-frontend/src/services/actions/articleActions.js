import { apiCall } from "../../api";
import {
  FETCH_ARTICLES_REQUEST,
  FETCH_ARTICLES_SUCCESS,
  FETCH_ARTICLES_FAILURE,
  BASE_URL,
} from "../constants/constants";
import axios from "axios";

const fetchArticlesRequest = () => {
  return {
    type: FETCH_ARTICLES_REQUEST,
  };
};

const fetchArticlesSuccess = (articles) => {
  return {
    type: FETCH_ARTICLES_SUCCESS,
    payload: articles,
  };
};

const fetchArticlesFailure = (error) => {
  return {
    type: FETCH_ARTICLES_FAILURE,
    payload: error,
  };
};

// Async action creator
const fetchArticles = (searchTerm, selectedSources, selectedCategories,selectedDate,page = 1) => {
  const queryParams = {
    search: searchTerm,
    source: selectedSources,
    category:selectedCategories,
    date: selectedDate,
    page:page
  };

  return async (dispatch) => {
   
    dispatch(fetchArticlesRequest());

    try {
      const response = await apiCall('api/get-articles', "GET", null, queryParams)
      // const response = await axios.get(
      //   `${BASE_URL}api/get-articles`,
      //   {
      //     params: {
      //       search: searchTerm,
      //       category: selectedCategories,
      //       source: selectedSources,
      //       date: null,
      //     },
      //   }
      // );

      const articles = response;
      console.log(articles,'article test ')

      dispatch(fetchArticlesSuccess(articles));
    } catch (error) {
      dispatch(fetchArticlesFailure(error.message));
    }
  };
};
export default fetchArticles;
